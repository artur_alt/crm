<?php

namespace App\DataFixtures;

use App\Entity\Company;
use App\Entity\Contact;
use App\Entity\Deal;
use App\Entity\Note;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private UserPasswordEncoderInterface $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        for($i = 0; $i < 3; $i++) {
            $this->createAppFixtures($manager, $i);
        }
    }

    private function createAppFixtures(ObjectManager $manager, int $index)
    {
        $company = (new Company())
            ->setName('Test company ' . $index)
            ->setCreatedAt(new DateTime())
            ->setUpdatedAt(new DateTime());

        $user = $this->createUser($company, $index);

        $manager->persist($user);
        $manager->persist($company->setUsers(new ArrayCollection([$user])));

        $contacts = [];
        for($i = 0; $i < 10; $i++) {
            $contact = $this->createContact($company, $index, $i);
            $manager->persist($contact);
            $contacts[] = $contact;
        }

        $deal = $this->createDeal($company, $index);
        $deal->setContacts(new ArrayCollection($contacts));
        $manager->persist($deal);

        for($i = 0; $i < 5; $i++) {
            $note = $this->createNote($index, $i);
            $note->setDeal($deal);
            $note->setUser($user);
            $manager->persist($note);
        }

        $manager->flush();
    }

    private function createUser(Company $company, int $index): User
    {
        $user = (new User())
            ->setCompanies(new ArrayCollection([$company]))
            ->setEmail(sprintf('test%d@test.test', $index))
            ->setFirstName('TestFirstName' . $index)
            ->setLastName('TestLastName' . $index)
            ->setRoles((0 === $index ? ['ROLE_ADMIN', 'ROLE_ADMIN_COMPANY'] : ['ROLE_ADMIN_COMPANY']))
            ->setApiToken(password_hash(time(), PASSWORD_DEFAULT))
            ->setApiTokenExpiredAt(new DateTime('+10000 day'))
            ->setUpdatedAt(new DateTime())
            ->setCreatedAt(new DateTime())
        ;

        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'the_new_password'
        ));

        return $user;
    }

    private function createContact(Company $company, int $dealIndex, int $index): Contact
    {
        return (new Contact($company))
            ->setName(sprintf('Contact %d %d', $dealIndex, $index))
            ->setEmail(sprintf('contact%d%d@test.test', $dealIndex, $index))
            ->setPhone('89771232323')
            ->setCreatedAt(new DateTime())
            ->setUpdatedAt(new DateTime())
            ->setAddress('Los Santos')
        ;
    }

    private function createDeal(Company $company, int $index): Deal
    {
        return (new Deal($company))
            ->setName('Deal ' . $index)
            ->setStatus('new')
            ->setCreatedAt(new DateTime())
            ->setUpdatedAt(new DateTime())
        ;
    }

    private function createNote(int $dealIndex, int $index): Note
    {
        return (new Note)
            ->setText(sprintf('Note %d %d', $dealIndex, $index))
            ->setCreatedAt(new DateTime())
            ->setUpdatedAt(new DateTime())
        ;
    }
}
