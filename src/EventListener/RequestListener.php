<?php


namespace App\EventListener;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class RequestListener
{
    public function onKernelRequest(RequestEvent $event)
    {
        if (!$event->isMasterRequest()) {
            // don't do anything if it's not the master request
            return;
        }

        $request = $event->getRequest();
        if ($request->getMethod() === Request::METHOD_POST) {
            /** @noinspection PhpComposerExtensionStubsInspection */
            $rowData = json_decode(file_get_contents("php://input"), true);

            if (is_array($rowData)) {
                $request->request->add($rowData);
            }
        }
    }
}
