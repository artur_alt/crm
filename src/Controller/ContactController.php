<?php


namespace App\Controller;


use App\Entity\Contact;
use App\Entity\Dto\Request\ContactDto as ContactRequest;
use App\Entity\User;
use App\Service\ContactService;
use JMS\Serializer\ArrayTransformerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ContactController
 * @package App\Controller
 *
 * @Route("/api/contact")
 */
class ContactController extends BaseController
{
    /**
     * @var ContactService
     */
    private ContactService $contactService;

    /**
     * ContactController constructor.
     * @param ArrayTransformerInterface $arrayTransformer
     * @param ValidatorInterface $validator
     * @param ContactService $contactService
     */
    public function __construct(
        ArrayTransformerInterface $arrayTransformer,
        ValidatorInterface $validator,
        ContactService $contactService
    ) {
        parent::__construct($arrayTransformer, $validator);
        $this->contactService = $contactService;
    }

    /**
     * @Route("/update", name="contact_update", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function update(Request $request): Response
    {
        /** @var User|null $user */
        $user = $this->getUser();
        if (null === $user) {
            return $this->json([], Response::HTTP_UNAUTHORIZED);
        }

        $contactRequest = $this->arrayTransformer->fromArray($request->request->all(), ContactRequest::class);
        $errorResponse = $this->validateRequest($contactRequest, 'update');
        if ($errorResponse) {
            return $this->json($this->arrayTransformer->toArray($errorResponse), Response::HTTP_BAD_REQUEST);
        }

        $contact = $this->getDoctrine()->getRepository(Contact::class)->find($contactRequest->getId());
        if (null === $contact || !$user->hasCompany($contact->getCompany()->getId())) {
            return $this->json($this->arrayTransformer->toArray($this->createErrorResponse(
                sprintf('Contact #%d not found', $contactRequest->getId())
            )),Response::HTTP_NOT_FOUND);
        }

        $this->contactService->updateContactFromRequest($contact, $contactRequest);
        $this->getDoctrine()->getManager()->persist($contact);
        $this->getDoctrine()->getManager()->flush();

        return $this->json($this->arrayTransformer->toArray($this->contactService->convertToResponse($contact)));
    }

    /**
     * @Route("/delete/{id}", name="contact_delete", methods={"POST"})
     *
     * @param int $id
     *
     * @return Response
     */
    public function delete(int $id): Response
    {
        /** @var User|null $user */
        $user = $this->getUser();
        if (null === $user) {
            return $this->json([], Response::HTTP_UNAUTHORIZED);
        }

        /** @var Contact|null $contact */
        $contact = $this->getDoctrine()->getRepository(Contact::class)->find($id);
        if (null === $contact || !$user->hasCompany($contact->getCompany()->getId())) {
            return $this->json($this->arrayTransformer->toArray($this->createErrorResponse(
                sprintf('Contact #%d not found', $id)
            )),Response::HTTP_NOT_FOUND);
        }

        $this->getDoctrine()->getManager()->remove($contact);
        $this->getDoctrine()->getManager()->flush();

        return new Response();
    }
}
