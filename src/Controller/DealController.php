<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Entity\Deal;
use App\Entity\Dto\Request\CreateDealRequest;
use App\Entity\Dto\Request\DealListRequest;
use App\Entity\Dto\Request\UpdateDealRequest;
use App\Entity\Note;
use App\Entity\User;
use App\Service\ContactService;
use App\Service\DealService;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\ArrayTransformerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class DealController
 * @package App\Controller
 *
 * @Route("/api/deal")
 */
class DealController extends BaseController
{
    /**
     * @var DealService
     */
    private DealService $dealService;

    /**
     * @var ContactService
     */
    private ContactService $contactService;

    /**
     * DealController constructor.
     * @param ArrayTransformerInterface $arrayTransformer
     * @param ValidatorInterface $validator
     * @param DealService $dealService
     * @param ContactService $contactService
     */
    public function __construct(
        ArrayTransformerInterface $arrayTransformer,
        ValidatorInterface $validator,
        DealService $dealService,
        ContactService $contactService
    ) {
        parent::__construct($arrayTransformer, $validator);
        $this->dealService = $dealService;
        $this->contactService = $contactService;
    }

    /**
     * @Route("", name="deal_list", methods={"GET"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function list(Request $request): Response
    {
        /** @var User|null $user */
        $user = $this->getUser();
        if (null === $user) {
            return $this->json([], Response::HTTP_UNAUTHORIZED);
        }

        /** @var DealListRequest $dealListRequest */
        $dealListRequest = $this->arrayTransformer->fromArray($request->query->all(), DealListRequest::class);
        $errorResponse = $this->validateRequest($dealListRequest);
        if ($errorResponse) {
            return $this->json($this->arrayTransformer->toArray($errorResponse), Response::HTTP_BAD_REQUEST);
        }

        if (!$user->hasCompany($dealListRequest->getCompanyId())) {
            return $this->json([]);
        }

        $deals = $this->getDoctrine()->getRepository(Deal::class)->findBy(
            ['company' => $dealListRequest->getCompanyId()],
            ['id' => 'ASC'],
            $dealListRequest->getLimit(),
            $dealListRequest->getOffset()
        );

        return $this->json($this->arrayTransformer->toArray(array_map(function (Deal $deal) {
            return $this->dealService->convertToResponse($deal);
        }, $deals)));
    }

    /**
     * @Route("/{id}", name="deal", methods={"GET"})
     *
     * @param int $id
     *
     * @return Response
     */
    public function getById(int $id): Response
    {
        /** @var User|null $user */
        $user = $this->getUser();
        if (null === $user) {
            return $this->json([], Response::HTTP_UNAUTHORIZED);
        }

        $deal = $this->getDoctrine()->getRepository(Deal::class)->findOneBy([
            'id' => $id,
            'company' => $user->getCompanies()->toArray(),
        ]);

        if (null === $deal) {
            return $this->json($this->arrayTransformer->toArray($this->createErrorResponse(
                sprintf('Deal #%d not found', $id)
            )),Response::HTTP_NOT_FOUND);
        }

        return $this->json($this->arrayTransformer->toArray($this->dealService->convertToResponse($deal)));
    }

    /**
     * @Route("/create", name="deal_create", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(Request $request): Response
    {
        /** @var User|null $user */
        $user = $this->getUser();
        if (null === $user) {
            return $this->json([], Response::HTTP_UNAUTHORIZED);
        }

        /** @var CreateDealRequest $createDealRequest */
        $createDealRequest = $this->arrayTransformer->fromArray($request->request->all(), CreateDealRequest::class);
        $errorResponse = $this->dealService->validateDealRequest($createDealRequest);
        if ($errorResponse) {
            return $this->json($this->arrayTransformer->toArray($errorResponse), Response::HTTP_BAD_REQUEST);
        }

        $company = $user->getCompanyById($createDealRequest->getCompanyId());
        if (null === $company) {
            return $this->json($this->arrayTransformer->toArray($this->createErrorResponse(
                sprintf('Company #%d not found', $createDealRequest->getCompanyId())
            )), Response::HTTP_NOT_FOUND);
        }

        $deal = (new Deal($company))
            ->setName($createDealRequest->getName())
            ->setStatus($createDealRequest->getStatus())
            ->setCreatedAt(new DateTime())
            ->setUpdatedAt(new DateTime());

        if (null !== $createDealRequest->getContacts()) {
            foreach ($createDealRequest->getContacts() as $contactDto) {
                if (null === $contactDto->getId()) {
                    $contact = $this->contactService->createContactFromRequest($company, $contactDto);
                    $this->getDoctrine()->getManager()->persist($contact);
                } else {
                    $contact = $this->getDoctrine()->getRepository(Contact::class)->find($contactDto->getId());
                    if (null === $contact) {
                        return $this->json($this->arrayTransformer->toArray($this->createErrorResponse(
                            sprintf('Contact #%d not found', $contactDto->getId())
                        )), Response::HTTP_NOT_FOUND);
                    }
                }

                $deal->addContact($contact);
            }
        }

        $note = (new Note)
            ->setText('The deal has been created.')
            ->setUser($user)
            ->setDeal($deal)
            ->setCreatedAt(new DateTime())
            ->setUpdatedAt(new DateTime());

        $this->getDoctrine()->getManager()->persist($note);
        $this->getDoctrine()->getManager()->persist($deal);
        $this->getDoctrine()->getManager()->flush();

        return $this->json($this->arrayTransformer->toArray($this->dealService->convertToResponse($deal)));
    }

    /**
     * @Route("/update", name="deal_update", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function update(Request $request): Response
    {
        /** @var User|null $user */
        $user = $this->getUser();
        if (null === $user) {
            return $this->json([], Response::HTTP_UNAUTHORIZED);
        }

        /** @var UpdateDealRequest $updateDealRequest */
        $updateDealRequest = $this->arrayTransformer->fromArray($request->request->all(), UpdateDealRequest::class);
        $errorResponse = $this->dealService->validateDealRequest($updateDealRequest);
        if ($errorResponse) {
            return $this->json($this->arrayTransformer->toArray($errorResponse), Response::HTTP_BAD_REQUEST);
        }

        $deal = $this->getDoctrine()->getRepository(Deal::class)->findOneBy([
            'id' => $updateDealRequest->getId(),
            'company' => $user->getCompanies()->toArray(),
        ]);

        if (null === $deal) {
            return $this->json($this->arrayTransformer->toArray($this->createErrorResponse(
                sprintf('Deal #%d not found', $updateDealRequest->getId())
            )),Response::HTTP_NOT_FOUND);
        }

        $company = $user->getCompanyById($updateDealRequest->getCompanyId());
        if (null === $company) {
            return $this->json($this->arrayTransformer->toArray($this->createErrorResponse(
                sprintf('Company #%d not found', $updateDealRequest->getCompanyId())
            )), Response::HTTP_NOT_FOUND);
        }

        // update contacts
        if (null !== $updateDealRequest->getContacts()) {
            $contacts = new ArrayCollection();
            foreach ($updateDealRequest->getContacts() as $contactDto) {
                if (null === $contactDto->getId()) {
                    $contact = $this->contactService->createContactFromRequest($company, $contactDto);
                    $this->getDoctrine()->getManager()->persist($contact);
                } else {
                    $contact = $this->getDoctrine()->getRepository(Contact::class)->findOneBy([
                        'id' => $contactDto->getId(),
                        'company' => $updateDealRequest->getCompanyId(),
                    ]);
                    if (null === $contact) {
                        return $this->json($this->arrayTransformer->toArray($this->createErrorResponse(
                            sprintf('Contact #%d not found', $contactDto->getId())
                        )),Response::HTTP_NOT_FOUND);
                    }
                }
                $contacts->add($contact);
            }
            $deal->setContacts($contacts);
        }

        // update deal
        $oldStatus = $deal->getStatus();
        $deal
            ->setName($updateDealRequest->getName())
            ->setStatus($updateDealRequest->getStatus())
            ->setUpdatedAt(new DateTime());

        if ($deal->getStatus() !== $oldStatus) {
            $note = (new Note)
                ->setText(sprintf("The status of the deal has been updated from '%s' to '%s'.", $oldStatus, $deal->getStatus()))
                ->setUser($user)
                ->setDeal($deal)
                ->setCreatedAt(new DateTime())
                ->setUpdatedAt(new DateTime());
            $this->getDoctrine()->getManager()->persist($note);
        }

        $this->getDoctrine()->getManager()->persist($deal);
        $this->getDoctrine()->getManager()->flush();

        return $this->json($this->arrayTransformer->toArray($this->dealService->convertToResponse($deal)));
    }
}
