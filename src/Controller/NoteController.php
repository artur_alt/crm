<?php


namespace App\Controller;


use App\Entity\Deal;
use App\Entity\Dto\Request\CreateNoteRequest;
use App\Entity\Dto\Response\NoteResponse;
use App\Entity\Note;
use App\Entity\User;
use DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class NoteController
 * @package App\Controller
 *
 * @Route("/api/note")
 */
class NoteController extends BaseController
{
    /**
     * @Route("/{dealId}", name="note_list", methods={"GET"})
     *
     * @param int|null $dealId
     * @param Request $request
     *
     * @return Response
     */
    public function list(Request $request, int $dealId): Response
    {
        /** @var User|null $user */
        $user = $this->getUser();
        if (null === $user) {
            return $this->json([], Response::HTTP_UNAUTHORIZED);
        }

        $limit = $request->query->get('limit', 100);
        $offset = $request->query->get('offset', 0);

        $deal = $this->getDoctrine()->getRepository(Deal::class)->findOneBy([
            'id' => $dealId,
            'company' => $user->getCompanies()->toArray(),
        ]);

        if (null === $deal) {
            return $this->json($this->arrayTransformer->toArray($this->createErrorResponse(
                sprintf('Deal #%d not found', $dealId)
            )),Response::HTTP_NOT_FOUND);
        }

        $notes = $this->getDoctrine()->getRepository(Note::class)->findBy([
            'deal' => $deal->getId(),
        ], ['id' => 'DESC'], $limit, $offset);

        return $this->json($this->arrayTransformer->toArray(array_map(function (Note $note) {
            return (new NoteResponse())
                ->setId($note->getId())
                ->setText($note->getText())
                ->setDealId($note->getDeal()->getId())
                ->setUserName(sprintf('%s %s', $note->getUser()->getFirstName(), $note->getUser()->getLastName()))
                ->setCreatedAt($note->getCreatedAt())
                ->setUpdatedAt($note->getUpdatedAt());
        }, $notes)));
    }

    /**
     * @Route("/create", name="note_create", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(Request $request): Response
    {
        /** @var User|null $user */
        $user = $this->getUser();
        if (null === $user) {
            return $this->json([], Response::HTTP_UNAUTHORIZED);
        }
        
        /** @var CreateNoteRequest $createNoteRequest */
        $createNoteRequest = $this->arrayTransformer->fromArray($request->request->all(), CreateNoteRequest::class);
        $errorResponse = $this->validateRequest($createNoteRequest);
        if ($errorResponse) {
            return $this->json($this->arrayTransformer->toArray($errorResponse), Response::HTTP_BAD_REQUEST);
        }

        $deal = $this->getDoctrine()->getRepository(Deal::class)->findOneBy([
            'id' => $createNoteRequest->getDealId(),
            'company' => $user->getCompanies()->toArray(),
        ]);

        if (null === $deal) {
            return $this->json($this->arrayTransformer->toArray($this->createErrorResponse(
                sprintf('Deal #%d not found', $createNoteRequest->getDealId())
            )),Response::HTTP_NOT_FOUND);
        }

        $note = (new Note)
            ->setText($createNoteRequest->getText())
            ->setUser($user)
            ->setDeal($deal)
            ->setCreatedAt(new DateTime())
            ->setUpdatedAt(new DateTime());

        $this->getDoctrine()->getManager()->persist($note);
        $this->getDoctrine()->getManager()->flush();

        $response = (new NoteResponse())
            ->setId($note->getId())
            ->setText($note->getText())
            ->setDealId($note->getDeal()->getId())
            ->setUserName(sprintf('%s %s', $user->getFirstName(), $user->getLastName()))
            ->setCreatedAt($note->getCreatedAt())
            ->setUpdatedAt($note->getUpdatedAt());

        return $this->json($this->arrayTransformer->toArray($response));
    }

    /**
     * @Route("/delete/{id}", name="note_delete", methods={"POST"})
     *
     * @param int $id
     *
     * @return Response
     */
    public function delete(int $id): Response
    {
        /** @var User|null $user */
        $user = $this->getUser();
        if (null === $user) {
            return $this->json([], Response::HTTP_UNAUTHORIZED);
        }

        $note = $this->getDoctrine()->getRepository(Note::class)->find($id);

        if (null === $note || !$user->hasCompany($note->getDeal()->getCompany()->getId())) {
            return $this->json($this->arrayTransformer->toArray($this->createErrorResponse(
                sprintf('Note #%d not found', $id)
            )),Response::HTTP_NOT_FOUND);
        }

        $this->getDoctrine()->getManager()->remove($note);
        $this->getDoctrine()->getManager()->flush();

        return new Response();
    }
}
