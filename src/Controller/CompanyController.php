<?php


namespace App\Controller;


use App\Entity\Company;
use App\Entity\Dto\Request\CreateCompanyRequest;
use App\Entity\Dto\Request\UpdateCompanyRequest;
use App\Entity\Dto\Request\UserDto;
use App\Entity\Dto\Response\CompanyDto;
use App\Entity\Dto\Response\UserResponse;
use App\Entity\User;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/company")
 */
class CompanyController extends BaseController
{
    /**
     * @Route("", name="company_list", methods={"GET"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function list(Request $request): Response
    {
        /** @var User|null $user */
        $user = $this->getUser();
        if (null === $user) {
            return $this->json([], Response::HTTP_UNAUTHORIZED);
        }

        $limit = $request->query->get('limit', 100);
        $offset = $request->query->get('offset', 0);

        if (in_array('ROLE_ADMIN', $user->getRoles())) {
            $companies = $this->getDoctrine()->getRepository(Company::class)->findBy(
                [],
                ['id' => 'ASC'],
                $limit,
                $offset
            );
        } else {
            $companies = $user->getCompanies()->toArray();
        }

        return $this->json($this->arrayTransformer->toArray(array_map(function(Company $company) {
            return $this->convertToResponse($company);
        }, $companies)));
    }

    /**
     * @Route("/create", name="company_create", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(Request $request): Response
    {
        /** @var User|null $user */
        $user = $this->getUser();
        if (null === $user) {
            return $this->json([], Response::HTTP_UNAUTHORIZED);
        }

        /** @var CreateCompanyRequest $createCompanyRequest */
        $createCompanyRequest = $this->arrayTransformer->fromArray($request->request->all(), CreateCompanyRequest::class);
        $errorResponse = $this->validateRequest($createCompanyRequest);
        if ($errorResponse) {
            return $this->json($this->arrayTransformer->toArray($errorResponse), Response::HTTP_BAD_REQUEST);
        }

        $company = (new Company())
            ->setName($createCompanyRequest->getName())
            ->setUsers(new ArrayCollection([$user]))
            ->setCreatedAt(new DateTime())
            ->setUpdatedAt(new DateTime());

        $this->getDoctrine()->getManager()->persist($company);
        $this->getDoctrine()->getManager()->flush();

        return $this->json($this->arrayTransformer->toArray($this->convertToResponse($company)));
    }

    /**
     * @Route("/update", name="company_update", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function update(Request $request): Response
    {
        /** @var User|null $user */
        $user = $this->getUser();
        if (null === $user) {
            return $this->json([], Response::HTTP_UNAUTHORIZED);
        }

        /** @var UpdateCompanyRequest $updateCompanyRequest */
        $updateCompanyRequest = $this->arrayTransformer->fromArray($request->request->all(), UpdateCompanyRequest::class);
        $errorResponse = $this->validateRequest($updateCompanyRequest);
        if ($errorResponse) {
            return $this->json($this->arrayTransformer->toArray($errorResponse), Response::HTTP_BAD_REQUEST);
        }

        if (!in_array(
            $user->getId(),
            array_map(fn(UserDto $userDto) => $userDto->getId(), $updateCompanyRequest->getUsers())
        )) {
            return $this->json($this->arrayTransformer->toArray($this->createErrorResponse(
                sprintf('Try to delete current user #%d from company', $user->getId())
            )), Response::HTTP_BAD_REQUEST);
        }

        $company = $user->getCompanyById($updateCompanyRequest->getId());
        if (null === $company) {
            return $this->json($this->arrayTransformer->toArray($this->createErrorResponse(
                sprintf('Company #%d not found', $updateCompanyRequest->getId())
            )), Response::HTTP_NOT_FOUND);
        }

        if ($updateCompanyRequest->getName()) {
            $company->setName($updateCompanyRequest->getName());
        }
        if (null !== $updateCompanyRequest->getUsers() || 0 !== count($updateCompanyRequest->getUsers())) {
            $userIds = array_unique(array_map(fn(UserDto $userDto) => $userDto->getId(), $updateCompanyRequest->getUsers()));

            $users = $this->getDoctrine()->getRepository(User::class)->findBy([
                'id' => $userIds,
            ]);

            if (count($userIds) !== count($users)) {
                return $this->json($this->arrayTransformer->toArray($this->createErrorResponse(
                    sprintf('Users %s not found', implode(', ', array_diff($userIds, array_map(function (User $u) {
                        return $u->getId();
                    }, $users))))
                )), Response::HTTP_NOT_FOUND);
            }
            $company->setUsers(new ArrayCollection($users));
        }

        $company->setUpdatedAt(new DateTime());

        $this->getDoctrine()->getManager()->persist($company);
        $this->getDoctrine()->getManager()->flush();

        return $this->json($this->arrayTransformer->toArray($this->convertToResponse($company)));
    }

    /**
     * @param Company $company
     *
     * @return CompanyDto
     */
    private function convertToResponse(Company $company): CompanyDto
    {
        $userResponseList = [];
        /** @var User $companyUser */
        foreach ($company->getUsers() as $companyUser) {
            $userResponseList[] = (new UserResponse())
                ->setId($companyUser->getId())
                ->setEmail($companyUser->getEmail())
                ->setPhone($companyUser->getPhone())
                ->setFirstName($companyUser->getFirstName())
                ->setLastName($companyUser->getLastName());
        }

        return (new CompanyDto())
            ->setId($company->getId())
            ->setName($company->getName())
            ->setUsers($userResponseList)
            ->setCreatedAt($company->getCreatedAt())
            ->setUpdatedAt($company->getUpdatedAt());
    }
}
