<?php


namespace App\Controller;


use App\Entity\Dto\Request\CreateUserRequest;
use App\Entity\Dto\Request\UserLoginRequest;
use App\Entity\Dto\Response\LoginResponse;
use App\Entity\Dto\Response\UserResponse;
use App\Entity\User;
use DateTime;
use Exception;
use JMS\Serializer\ArrayTransformerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class UserController
 * @package App\Controller
 *
 * @Route("/api/user")
 */
class UserController extends BaseController
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $passwordEncoder;

    /**
     * UserController constructor.
     * @param ArrayTransformerInterface $arrayTransformer
     * @param ValidatorInterface $validator
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(
        ArrayTransformerInterface $arrayTransformer,
        ValidatorInterface $validator,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        parent::__construct($arrayTransformer, $validator);
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @Route("/login", name="user_login", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function login(Request $request): Response
    {
        /** @var UserLoginRequest $userLoginRequest */
        $userLoginRequest = $this->arrayTransformer->fromArray($request->request->all(), UserLoginRequest::class);
        $errorResponse = $this->validateRequest($userLoginRequest);
        if ($errorResponse) {
            return $this->json($this->arrayTransformer->toArray($errorResponse), Response::HTTP_BAD_REQUEST);
        }

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'email' => $userLoginRequest->getEmail(),
        ]);

        if (null === $user) {
            return $this->json($this->arrayTransformer->toArray($this->createErrorResponse(
                'Username could not be found.'
            )), Response::HTTP_UNAUTHORIZED);
        }

        if (!$this->passwordEncoder->isPasswordValid($user, $userLoginRequest->getPassword())) {
            return $this->json($this->arrayTransformer->toArray($this->createErrorResponse(
                'Username could not be found.'
            )), Response::HTTP_UNAUTHORIZED);
        }

        $user->setApiToken($this->createRandomToken());
        $user->setApiTokenExpiredAt(new DateTime('+30min'));
        $user->setUpdatedAt(new DateTime());

        $this->getDoctrine()->getManager()->persist($user);
        $this->getDoctrine()->getManager()->flush();

        $loginResponse = (new LoginResponse())
            ->setToken($user->getApiToken())
            ->setTokenExpiredAt($user->getApiTokenExpiredAt());

        return $this->json($this->arrayTransformer->toArray($loginResponse));
    }

    /**
     * @Route("/create", name="user_create", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws Exception
     */
    public function create(Request $request): Response
    {
        /** @var User|null $currentUser */
        $currentUser = $this->getUser();
        if (null === $currentUser) {
            return $this->json([], Response::HTTP_UNAUTHORIZED);
        }

        /** @var CreateUserRequest $createUserRequest */
        $createUserRequest = $this->arrayTransformer->fromArray($request->request->all(), CreateUserRequest::class);
        $errorResponse = $this->validateRequest($createUserRequest);
        if ($errorResponse) {
            return $this->json($this->arrayTransformer->toArray($errorResponse), Response::HTTP_BAD_REQUEST);
        }

        $oldUser = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'email' => $createUserRequest->getEmail(),
        ]);

        if ($oldUser) {
            return $this->json($this->arrayTransformer->toArray($this->createErrorResponse(
                'User already exists'
            )), Response::HTTP_BAD_REQUEST);
        }

        $user = (new User())
            ->setEmail($createUserRequest->getEmail())
            ->setFirstName($createUserRequest->getFirstName())
            ->setLastName($createUserRequest->getLastName())
            ->setPhone($createUserRequest->getPhone())
            ->setApiToken($this->createRandomToken())
            ->setApiTokenExpiredAt(new DateTime('+30min'))
            ->setUpdatedAt(new DateTime())
            ->setCreatedAt(new DateTime());

        $user->setPassword($this->passwordEncoder->encodePassword($user, $createUserRequest->getPassword()));

        $this->getDoctrine()->getManager()->persist($user);
        $this->getDoctrine()->getManager()->flush();

        $userResponse = (new UserResponse())
            ->setId($user->getId())
            ->setEmail($user->getEmail())
            ->setFirstName($user->getFirstName())
            ->setLastName($user->getLastName())
            ->setPhone($user->getPhone())
            ->setCreatedAt($user->getCreatedAt())
            ->setUpdatedAt($user->getUpdatedAt());

        return $this->json($this->arrayTransformer->toArray($userResponse));
    }

    /**
     * @return string
     */
    private function createRandomToken(): string
    {
        return base_convert(sha1(uniqid(mt_rand(), true)), 16, 36)
            . password_hash(mt_rand(), PASSWORD_DEFAULT);
    }
}
