<?php


namespace App\Controller;


use App\Entity\Dto\Request\RequestInterface;
use App\Entity\Dto\Response\ErrorDto;
use App\Entity\Dto\Response\ErrorResponse;
use JMS\Serializer\ArrayTransformerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Validator\Constraints\GroupSequence;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BaseController extends AbstractController
{
    /**
     * @var ArrayTransformerInterface
     */
    protected ArrayTransformerInterface $arrayTransformer;

    /**
     * @var ValidatorInterface
     */
    protected ValidatorInterface $validator;

    /**
     * @param ArrayTransformerInterface $arrayTransformer
     * @param ValidatorInterface $validator
     */
    public function __construct(ArrayTransformerInterface $arrayTransformer, ValidatorInterface $validator)
    {
        $this->validator = $validator;
        $this->arrayTransformer = $arrayTransformer;
    }

    /**
     * @param RequestInterface $request
     * @param string|GroupSequence|(string|GroupSequence)[]|null $groups
     *
     * @return ErrorResponse|null
     */
    public function validateRequest(RequestInterface $request, $groups = null): ?ErrorResponse
    {
        $errors = $this->validator->validate($request, null, $groups);
        if (0 === $errors->count()) {
            return null;
        }

        $errorList = [];
        foreach ($errors as $violation) {
            $errorList[] = (new ErrorDto())
                ->setMessage(sprintf('`%s` %s', $violation->getPropertyPath(), $violation->getMessage()));
        }

        return (new ErrorResponse())->setErrors($errorList);
    }

    /**
     * @param string $message
     *
     * @return ErrorResponse
     */
    public function createErrorResponse(string $message): ErrorResponse
    {
        return (new ErrorResponse())->setErrors([
            (new ErrorDto())->setMessage($message)
        ]);
    }
}
