<?php


namespace App\Service;


use App\Entity\Company;
use App\Entity\Contact;
use App\Entity\Dto\Request\ContactDto as ContactRequest;
use App\Entity\Dto\Response\ContactDto as ContactResponse;
use DateTime;

class ContactService
{
    /**
     * @param Contact $contact
     * @param ContactRequest $contactRequest
     */
    public function updateContactFromRequest(Contact $contact, ContactRequest $contactRequest): void
    {
        if ($contactRequest->getName()) {
            $contact->setName($contactRequest->getName());
        }
        if ($contactRequest->getAddress()) {
            $contact->setAddress($contactRequest->getAddress());
        }
        if ($contactRequest->getPhone()) {
            $contact->setPhone($contactRequest->getPhone());
        }
        if ($contactRequest->getEmail()) {
            $contact->setEmail($contactRequest->getEmail());
        }
        if (
            $contactRequest->getName() || $contactRequest->getAddress()
            || $contactRequest->getPhone() || $contactRequest->getEmail()
        ) {
            $contact->setUpdatedAt(new DateTime());
        }
    }

    /**
     * @param Contact $contact
     *
     * @return ContactResponse
     */
    public function convertToResponse(Contact $contact): ContactResponse
    {
        return (new ContactResponse())
            ->setId($contact->getId())
            ->setName($contact->getName())
            ->setAddress($contact->getAddress())
            ->setPhone($contact->getPhone())
            ->setEmail($contact->getEmail())
            ->setUpdatedAt($contact->getUpdatedAt())
            ->setCreatedAt($contact->getCreatedAt());
    }

    /**
     * @param Company $company
     * @param ContactRequest $contactRequest
     *
     * @return Contact
     */
    public function createContactFromRequest(Company $company, ContactRequest $contactRequest): Contact
    {
        return (new Contact($company))
            ->setName($contactRequest->getName())
            ->setPhone($contactRequest->getPhone())
            ->setEmail($contactRequest->getEmail())
            ->setAddress($contactRequest->getAddress())
            ->setCreatedAt(new DateTime())
            ->setUpdatedAt(new DateTime());
    }
}
