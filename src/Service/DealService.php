<?php


namespace App\Service;


use App\Entity\Deal;
use App\Entity\Dto\Request\DealRequestInterface;
use App\Entity\Dto\Request\RequestInterface;
use App\Entity\Dto\Response\ContactDto;
use App\Entity\Dto\Response\DealResponse;
use App\Entity\Dto\Response\ErrorDto;
use App\Entity\Dto\Response\ErrorResponse;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DealService
{
    /**
     * @var ValidatorInterface
     */
    private ValidatorInterface $validator;

    /**
     * DealService constructor.
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param Deal $deal
     *
     * @return DealResponse
     */
    public function convertToResponse(Deal $deal): DealResponse
    {
        $contactsResponse = [];
        foreach ($deal->getContacts() as $contact) {
            $contactsResponse[] = (new ContactDto())
                ->setId($contact->getId())
                ->setName($contact->getName())
                ->setPhone($contact->getPhone())
                ->setEmail($contact->getEmail())
                ->setAddress($contact->getAddress())
                ->setCreatedAt($contact->getCreatedAt())
                ->setUpdatedAt($contact->getUpdatedAt())
            ;
        }

        return (new DealResponse)
            ->setId($deal->getId())
            ->setName($deal->getName())
            ->setStatus($deal->getStatus())
            ->setCompanyId($deal->getCompany()->getId())
            ->setCreatedAt($deal->getCreatedAt())
            ->setUpdatedAt($deal->getUpdatedAt())
            ->setContacts($contactsResponse)
        ;
    }

    /**
     * @param RequestInterface $dealRequest
     *
     * @return ErrorResponse|null
     */
    public function validateDealRequest(RequestInterface $dealRequest): ?ErrorResponse
    {
        $errors = $this->validator->validate($dealRequest);
        if ($dealRequest instanceof DealRequestInterface && null !== $dealRequest->getContacts()) {
            foreach ($dealRequest->getContacts() as $contactDto) {
                if (null === $contactDto->getId()) {
                    $errors->addAll($this->validator->validate($contactDto));
                }
            }
        }

        if (0 === $errors->count()) {
            return null;
        }

        $errorList = [];
        foreach ($errors as $violation) {
            $errorList[] = (new ErrorDto())
                ->setMessage(sprintf('`%s` %s', $violation->getPropertyPath(), $violation->getMessage()));
        }

        return (new ErrorResponse())->setErrors($errorList);
    }
}
