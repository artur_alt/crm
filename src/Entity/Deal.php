<?php

namespace App\Entity;

use App\Repository\DealRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DealRepository::class)
 */
class Deal
{
    public const NEW_STATUS = 'new';
    public const HOT_STATUS = 'hot';
    public const SOLD_STATUS = 'sold';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @var Company
     *
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="deals")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    private Company $company;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private string $status;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $name;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Contact", inversedBy="deals")
     * @ORM\JoinTable(name="deals_contacts")
     */
    private Collection $contacts;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Note", mappedBy="deal")
     */
    private Collection $notes;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(type="datetime")
     */
    private ?DateTime $createdAt;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(type="datetime")
     */
    private ?DateTime $updatedAt;

    /**
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->contacts = new ArrayCollection();
        $this->notes = new ArrayCollection();
        $this->company = $company;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->company;
    }

    /**
     * @param Company $company
     *
     * @return $this
     */
    public function setCompany(Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return $this
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return $this
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Contact[]
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    /**
     * @param Contact $contact
     *
     * @return $this
     */
    public function addContact(Contact $contact): self
    {
        $this->contacts->add($contact);

        return $this;
    }

    /**
     * @param ArrayCollection $contacts
     *
     * @return $this
     */
    public function setContacts(ArrayCollection $contacts): self
    {
        $this->contacts = $contacts;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getNotes(): Collection
    {
        return $this->notes;
    }

    /**
     * @param Note $note
     *
     * @return $this
     */
    public function addNote(Note $note): self
    {
        $this->notes->add($note);

        return $this;
    }

    /**
     * @param ArrayCollection $notes
     *
     * @return $this
     */
    public function setNotes(ArrayCollection $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime|null $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(?DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime|null $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt(?DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return string[]
     */
    public static function getStatuses(): array
    {
        return [self::NEW_STATUS, self::HOT_STATUS, self::SOLD_STATUS];
    }
}
