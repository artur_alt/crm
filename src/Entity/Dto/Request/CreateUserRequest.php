<?php


namespace App\Entity\Dto\Request;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class CreateUserRequest implements RequestInterface
{
    /**
     * @var string|null
     *
     * @Assert\Email()
     * @Assert\NotBlank
     *
     * @Serializer\Type("string")
     */
    private ?string $email;

    /**
     * @var string|null
     *
     * @Assert\Type("string")
     * @Assert\NotBlank
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("firstName")
     */
    private ?string $firstName;

    /**
     * @var string|null
     *
     * @Assert\Type("string")
     * @Assert\NotBlank
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("lastName")
     */
    private ?string $lastName;

    /**
     * @var string|null
     *
     * @Assert\Type("string")
     * @Assert\NotBlank
     *
     * @Serializer\Type("string")
     */
    private ?string $phone;

    /**
     * @var string|null
     *
     * @Assert\Type("string")
     * @Assert\NotBlank
     *
     * @Serializer\Type("string")
     */
    private ?string $password;

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     *
     * @return $this
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     *
     * @return $this
     */
    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     *
     * @return $this
     */
    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     *
     * @return $this
     */
    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     *
     * @return $this
     */
    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }
}
