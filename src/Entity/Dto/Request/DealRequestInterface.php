<?php


namespace App\Entity\Dto\Request;


interface DealRequestInterface
{
    /**
     * @return ContactDto[]|null
     */
    public function getContacts(): ?array;
}
