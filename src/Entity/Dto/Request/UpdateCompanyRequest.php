<?php


namespace App\Entity\Dto\Request;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateCompanyRequest implements RequestInterface
{
    /**
     * @var int|null
     *
     * @Assert\Type("integer")
     * @Assert\NotBlank
     *
     * @Serializer\Type("integer")
     */
    private ?int $id;

    /**
     * @var string|null
     *
     * @Assert\Type("string")
     *
     * @Serializer\Type("string")
     */
    private ?string $name;

    /**
     * @var UserDto[]|null
     *
     * @Serializer\Type("array<App\Entity\Dto\Request\UserDto>")
     */
    private ?array $users;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     *
     * @return $this
     */
    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return $this
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return UserDto[]|null
     */
    public function getUsers(): ?array
    {
        return $this->users;
    }

    /**
     * @param UserDto[]|null $users
     *
     * @return $this
     */
    public function setUsers(?array $users): self
    {
        $this->users = $users;

        return $this;
    }
}
