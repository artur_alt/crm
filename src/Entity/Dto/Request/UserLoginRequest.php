<?php


namespace App\Entity\Dto\Request;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class UserLoginRequest implements RequestInterface
{
    /**
     * @var string|null
     *
     * @Assert\Email()
     * @Assert\NotBlank
     *
     * @Serializer\Type("string")
     */
    private ?string $email;

    /**
     * @var string|null
     *
     * @Assert\Type("string")
     * @Assert\NotBlank
     *
     * @Serializer\Type("string")
     */
    private ?string $password;

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     *
     * @return $this
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     *
     * @return $this
     */
    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }
}
