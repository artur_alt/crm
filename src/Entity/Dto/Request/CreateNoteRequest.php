<?php


namespace App\Entity\Dto\Request;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class CreateNoteRequest implements RequestInterface
{
    /**
     * @var string|null
     *
     * @Assert\Type("string")
     * @Assert\NotBlank
     *
     * @Serializer\Type("string")
     */
    private ?string $text;

    /**
     * @var int|null
     *
     * @Assert\Type("integer")
     * @Assert\NotBlank
     *
     * @Serializer\Type("integer")
     */
    private ?int $dealId;

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     *
     * @return $this
     */
    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getDealId(): ?int
    {
        return $this->dealId;
    }

    /**
     * @param int|null $dealId
     *
     * @return $this
     */
    public function setDealId(?int $dealId): self
    {
        $this->dealId = $dealId;

        return $this;
    }
}
