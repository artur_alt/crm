<?php


namespace App\Entity\Dto\Request;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class CreateDealRequest implements DealRequestInterface, RequestInterface
{
    /**
     * @var string|null
     *
     * @Assert\Type("string")
     * @Assert\NotBlank
     *
     * @Serializer\Type("string")
     */
    protected $name;

    /**
     * @var string|null
     *
     * @Assert\Type("string")
     * @Assert\NotBlank
     * @Assert\Choice(callback={"\App\Entity\Deal", "getStatuses"})
     *
     * @Serializer\Type("string")
     */
    protected $status;

    /**
     * @var ContactDto[]|null
     *
     * @Serializer\Type("array<App\Entity\Dto\Request\ContactDto>")
     */
    private $contacts;

    /**
     * @var int|null
     *
     * @Assert\Type("integer")
     * @Assert\NotBlank
     *
     * @Serializer\Type("integer")
     * @Serializer\SerializedName("companyId")
     */
    private $companyId;

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return $this
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     *
     * @return $this
     */
    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return ContactDto[]|null
     */
    public function getContacts(): ?array
    {
        return $this->contacts;
    }

    /**
     * @param array|null $contacts
     *
     * @return $this
     */
    public function setContacts(?array $contacts): self
    {
        $this->contacts = $contacts;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCompanyId(): ?int
    {
        return $this->companyId;
    }

    /**
     * @param int|null $companyId
     *
     * @return $this
     */
    public function setCompanyId(?int $companyId): self
    {
        $this->companyId = $companyId;

        return $this;
    }
}
