<?php


namespace App\Entity\Dto\Request;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class ContactDto implements RequestInterface
{
    /**
     * @var int|null
     *
     * @Assert\Type("integer", groups={"Default", "update"})
     *
     * @Serializer\Type("integer")
     */
    protected $id;

    /**
     * @var string|null
     *
     * @Assert\Type("string", groups={"Default", "update"})
     * @Assert\NotBlank
     *
     * @Serializer\Type("string")
     */
    protected $name;

    /**
     * @var string|null
     *
     * @Assert\Type("string", groups={"Default", "update"})
     *
     * @Serializer\Type("string")
     */
    protected $address;

    /**
     * @var string|null
     *
     * @Assert\Type("string", groups={"Default", "update"})
     * @Assert\NotBlank
     *
     * @Serializer\Type("string")
     */
    protected $phone;

    /**
     * @var string|null
     *
     * @Assert\Email(groups={"Default", "update"})
     * @Assert\NotBlank
     *
     * @Serializer\Type("string")
     */
    protected $email;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     *
     * @return $this
     */
    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return $this
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     *
     * @return $this
     */
    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     *
     * @return $this
     */
    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     *
     * @return $this
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }
}
