<?php


namespace App\Entity\Dto\Request;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class DealListRequest implements RequestInterface
{
    /**
     * @var int
     *
     * @Serializer\Type("integer")
     * @Serializer\SerializedName("limit")
     */
    private int $limit = 100;

    /**
     * @var int
     *
     * @Serializer\Type("integer")
     * @Serializer\SerializedName("offset")
     */
    private int $offset = 0;

    /**
     * @var int|null
     *
     * @Assert\NotBlank()
     *
     * @Serializer\Type("integer")
     * @Serializer\SerializedName("companyId")
     */
    private ?int $companyId;

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     *
     * @return $this
     */
    public function setLimit(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     *
     * @return $this
     */
    public function setOffset(int $offset): self
    {
        $this->offset = $offset;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCompanyId(): ?int
    {
        return $this->companyId;
    }

    /**
     * @param int $companyId
     *
     * @return $this
     */
    public function setCompanyId(int $companyId): self
    {
        $this->companyId = $companyId;

        return $this;
    }
}
