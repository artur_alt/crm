<?php


namespace App\Entity\Dto\Response;


use JMS\Serializer\Annotation as Serializer;

class ErrorResponse
{
    /**
     * @var ErrorDto[]
     *
     * @Serializer\Type("array<App\Entity\Dto\Response\ErrorDto>")
     * @Serializer\SerializedName("errors")
     */
    private $errors = [];

    /**
     * @return ErrorDto[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param ErrorDto[] $errors
     *
     * @return $this
     */
    public function setErrors(array $errors): self
    {
        $this->errors = $errors;

        return $this;
    }
}
