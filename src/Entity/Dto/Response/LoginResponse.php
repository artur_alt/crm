<?php


namespace App\Entity\Dto\Response;


use DateTime;
use JMS\Serializer\Annotation as Serializer;

class LoginResponse
{
    /**
     * @var string|null
     *
     * @Serializer\Type("string")
     */
    protected $token;

    /**
     * @var DateTime|null
     *
     * @Serializer\Type("DateTime")
     */
    protected $tokenExpiredAt;

    /**
     * @return string|null
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @param string|null $token
     *
     * @return $this
     */
    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getTokenExpiredAt(): ?DateTime
    {
        return $this->tokenExpiredAt;
    }

    /**
     * @param DateTime|null $tokenExpiredAt
     *
     * @return $this
     */
    public function setTokenExpiredAt(?DateTime $tokenExpiredAt): self
    {
        $this->tokenExpiredAt = $tokenExpiredAt;

        return $this;
    }
}
