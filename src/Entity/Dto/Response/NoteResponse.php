<?php


namespace App\Entity\Dto\Response;

use DateTime;
use JMS\Serializer\Annotation as Serializer;

class NoteResponse
{
    /**
     * @var int|null
     *
     * @Serializer\Type("integer")
     */
    protected ?int $id;

    /**
     * @var string|null
     *
     * @Serializer\Type("string")
     */
    protected ?string $text;

    /**
     * @var string|null
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("userName")
     */
    protected ?string $userName;

    /**
     * @var int|null
     *
     * @Serializer\Type("integer")
     * @Serializer\SerializedName("dealId")
     */
    protected ?int $dealId;

    /**
     * @var DateTime|null
     *
     * @Serializer\Type("DateTime")
     * @Serializer\SerializedName("createdAt")
     */
    protected ?DateTime $createdAt;

    /**
     * @var DateTime|null
     *
     * @Serializer\Type("DateTime")
     * @Serializer\SerializedName("updatedAt")
     */
    protected ?DateTime $updatedAt;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     *
     * @return $this
     */
    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     *
     * @return $this
     */
    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUserName(): ?string
    {
        return $this->userName;
    }

    /**
     * @param string|null $userName
     *
     * @return $this
     */
    public function setUserName(?string $userName): self
    {
        $this->userName = $userName;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getDealId(): ?int
    {
        return $this->dealId;
    }

    /**
     * @param int|null $dealId
     *
     * @return $this
     */
    public function setDealId(?int $dealId): self
    {
        $this->dealId = $dealId;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime|null $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(?DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime|null $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt(?DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
