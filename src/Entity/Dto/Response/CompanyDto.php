<?php


namespace App\Entity\Dto\Response;


use DateTime;
use JMS\Serializer\Annotation as Serializer;

class CompanyDto
{
    /**
     * @var int|null
     *
     * @Serializer\Type("integer")
     */
    protected $id;

    /**
     * @var string|null
     *
     * @Serializer\Type("string")
     */
    protected $name;

    /**
     * @var UserResponse[]
     *
     * @Serializer\Type("array<App\Entity\Dto\Response\UserResponse>")
     */
    protected $users = [];

    /**
     * @var DateTime|null
     *
     * @Serializer\Type("DateTime")
     * @Serializer\SerializedName("createdAt")
     */
    protected $createdAt;

    /**
     * @var DateTime|null
     *
     * @Serializer\Type("DateTime")
     * @Serializer\SerializedName("updatedAt")
     */
    protected $updatedAt;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     *
     * @return $this
     */
    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return $this
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return UserResponse[]
     */
    public function getUsers(): array
    {
        return $this->users;
    }

    /**
     * @param UserResponse[] $users
     *
     * @return $this
     */
    public function setUsers(array $users): self
    {
        $this->users = $users;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime|null $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(?DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime|null $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt(?DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
