<?php


namespace App\Entity\Dto\Response;


use JMS\Serializer\Annotation as Serializer;

class ErrorDto
{
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("message")
     */
    private $message = 'Sayonara! <(^__^)>';

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return $this
     */
    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }
}
