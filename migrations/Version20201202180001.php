<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201202180001 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Table "contact"';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE SEQUENCE contact_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('
            CREATE TABLE contact
            (
                id         INT                            NOT NULL,
                name       VARCHAR(1000)                  NOT NULL,
                address    VARCHAR(1000) DEFAULT NULL,
                phone      VARCHAR(255)  DEFAULT NULL,
                email      VARCHAR(255)  DEFAULT NULL,
                created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
                updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
                PRIMARY KEY (id)
            )
        ');
        $this->addSql('
            CREATE TABLE deals_contacts
            (
                deal_id    INT NOT NULL,
                contact_id INT NOT NULL,
                PRIMARY KEY (deal_id, contact_id)
            )
        ');
        $this->addSql('CREATE INDEX IDX_58D84E06F60E2305 ON deals_contacts (deal_id)');
        $this->addSql('CREATE INDEX IDX_58D84E06E7A1254A ON deals_contacts (contact_id)');
        $this->addSql('
            ALTER TABLE deals_contacts
                ADD CONSTRAINT FK_58D84E06F60E2305 FOREIGN KEY (deal_id) REFERENCES deal (id) NOT DEFERRABLE INITIALLY IMMEDIATE
        ');
        $this->addSql('
            ALTER TABLE deals_contacts 
                ADD CONSTRAINT FK_58D84E06E7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id) NOT DEFERRABLE INITIALLY IMMEDIATE
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE deals_contacts DROP CONSTRAINT FK_58D84E06E7A1254A');
        $this->addSql('DROP SEQUENCE contact_id_seq CASCADE');
        $this->addSql('DROP TABLE contact');
        $this->addSql('DROP TABLE deals_contacts');
    }
}
