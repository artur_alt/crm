<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210310182329 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE SEQUENCE company_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE company (id INT NOT NULL, name VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE companies_users (company_id INT NOT NULL, user_id INT NOT NULL, PRIMARY KEY(company_id, user_id))');
        $this->addSql('CREATE INDEX IDX_F70AEA0D979B1AD6 ON companies_users (company_id)');
        $this->addSql('CREATE INDEX IDX_F70AEA0DA76ED395 ON companies_users (user_id)');
        $this->addSql('ALTER TABLE companies_users ADD CONSTRAINT FK_F70AEA0D979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE companies_users ADD CONSTRAINT FK_F70AEA0DA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE contact ADD company_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contact ADD CONSTRAINT FK_4C62E638979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_4C62E638979B1AD6 ON contact (company_id)');
        $this->addSql('ALTER TABLE deal ADD company_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE deal ADD CONSTRAINT FK_E3FEC116979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E3FEC116979B1AD6 ON deal (company_id)');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE companies_users DROP CONSTRAINT FK_F70AEA0D979B1AD6');
        $this->addSql('ALTER TABLE contact DROP CONSTRAINT FK_4C62E638979B1AD6');
        $this->addSql('ALTER TABLE deal DROP CONSTRAINT FK_E3FEC116979B1AD6');
        $this->addSql('DROP SEQUENCE company_id_seq CASCADE');
        $this->addSql('DROP TABLE company');
        $this->addSql('DROP TABLE companies_users');
        $this->addSql('DROP INDEX IDX_4C62E638979B1AD6');
        $this->addSql('ALTER TABLE contact DROP company_id');
        $this->addSql('DROP INDEX IDX_E3FEC116979B1AD6');
        $this->addSql('ALTER TABLE deal DROP company_id');
    }
}
