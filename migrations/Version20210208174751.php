<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210208174751 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE deals_contacts DROP CONSTRAINT FK_58D84E06F60E2305');
        $this->addSql('ALTER TABLE deals_contacts DROP CONSTRAINT FK_58D84E06E7A1254A');
        $this->addSql('ALTER TABLE deals_contacts ADD CONSTRAINT FK_58D84E06F60E2305 FOREIGN KEY (deal_id) REFERENCES deal (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE deals_contacts ADD CONSTRAINT FK_58D84E06E7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE deals_contacts DROP CONSTRAINT fk_58d84e06f60e2305');
        $this->addSql('ALTER TABLE deals_contacts DROP CONSTRAINT fk_58d84e06e7a1254a');
        $this->addSql('ALTER TABLE deals_contacts ADD CONSTRAINT fk_58d84e06f60e2305 FOREIGN KEY (deal_id) REFERENCES deal (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE deals_contacts ADD CONSTRAINT fk_58d84e06e7a1254a FOREIGN KEY (contact_id) REFERENCES contact (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
