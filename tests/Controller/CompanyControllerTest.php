<?php


namespace App\Tests\Controller;


use App\Entity\Company;
use App\Entity\User;
use App\Repository\CompanyRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CompanyControllerTest extends WebTestCase
{
    public function testListUnauthorized()
    {
        $client = static::createClient();
        $client->request('GET', '/api/company');

        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testList()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test1@test.test']);
        $client->loginUser($testUser);

        $client->request(
            'GET',
            '/api/company',
            ['limit' => 3, 'offset' => 0],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertStringContainsString('"id":', $client->getResponse()->getContent());
        $this->assertStringContainsString('"name":"', $client->getResponse()->getContent());
        $this->assertStringContainsString('"users":', $client->getResponse()->getContent());
    }

    public function testListForAdmin()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $client->loginUser($testUser);

        $client->request(
            'GET',
            '/api/company',
            [],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertStringContainsString('"id":', $client->getResponse()->getContent());
        $this->assertStringContainsString('"name":"Test company 0"', $client->getResponse()->getContent());
        $this->assertStringContainsString('"name":"Test company 1"', $client->getResponse()->getContent());
        $this->assertStringContainsString('"users":', $client->getResponse()->getContent());
    }

    public function testCreate()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test2@test.test']);
        $client->loginUser($testUser);

        $client->request(
            'POST',
            '/api/company/create',
            [
                'name' => 'Lol new company 123'
            ],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertStringContainsString('"id":', $client->getResponse()->getContent());
        $this->assertStringContainsString('"name":"Lol new company 123"', $client->getResponse()->getContent());
        $this->assertStringContainsString('"users":', $client->getResponse()->getContent());
    }

    public function testCreateBadRequest()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test2@test.test']);
        $client->loginUser($testUser);

        $client->request(
            'POST',
            '/api/company/create',
            [],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testCreateUnauthorized()
    {
        $client = static::createClient();
        $client->request('POST', '/api/company/create');

        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testUpdate()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        $companyRepository = static::$container->get(CompanyRepository::class);

        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test2@test.test']);
        $client->loginUser($testUser);

        /** @var Company $company */
        $company = $companyRepository->findOneBy([], ['id' => 'DESC']);
        $newName = $company->getName() . ' lol';

        $client->request(
            'POST',
            '/api/company/update',
            [
                'id' => $company->getId(),
                'name' => $newName,
                'users' => [['id' => $testUser->getId()]],
            ],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertStringContainsString('"id":', $client->getResponse()->getContent());
        $this->assertStringContainsString(sprintf('"name":"%s"', $newName), $client->getResponse()->getContent());
        $this->assertStringContainsString('"users":', $client->getResponse()->getContent());
    }

    public function testUpdateBanzai()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        $companyRepository = static::$container->get(CompanyRepository::class);

        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test2@test.test']);
        $client->loginUser($testUser);

        /** @var Company $company */
        $company = $companyRepository->findOneBy([], ['id' => 'DESC']);
        $newName = $company->getName() . ' lol';

        $client->request(
            'POST',
            '/api/company/update',
            [
                'id' => $company->getId(),
                'name' => $newName,
                'users' => [],
            ],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testUpdateBadRequest()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);

        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test2@test.test']);
        $client->loginUser($testUser);

        $client->request(
            'POST',
            '/api/company/update',
            [],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testUpdateNotFound()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        $companyRepository = static::$container->get(CompanyRepository::class);

        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test2@test.test']);
        $client->loginUser($testUser);

        /** @var Company $company */
        $company = $companyRepository->findOneBy([], ['id' => 'DESC']);
        $newName = $company->getName() . ' lol';

        $client->request(
            'POST',
            '/api/company/update',
            [
                'id' => $company->getId() + 1,
                'name' => $newName,
                'users' => [['id' => $testUser->getId()]],
            ],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testUpdateUserNotFound()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        $companyRepository = static::$container->get(CompanyRepository::class);

        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test2@test.test']);
        /** @var User $testUser */
        $testUser2 = $userRepository->findOneBy([], ['id' => 'DESC']);
        $client->loginUser($testUser);

        /** @var Company $company */
        $company = $companyRepository->findOneBy([], ['id' => 'DESC']);
        $newName = $company->getName() . ' lol';

        $client->request(
            'POST',
            '/api/company/update',
            [
                'id' => $company->getId(),
                'name' => $newName,
                'users' => [
                    ['id' => $testUser->getId()],
                    ['id' => $testUser2->getId() + 1],
                ],
            ],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }
}
