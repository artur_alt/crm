<?php


namespace App\Tests\Controller;


use App\Entity\Deal;
use App\Entity\Note;
use App\Entity\User;
use App\Repository\DealRepository;
use App\Repository\NoteRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class NoteControllerTest extends WebTestCase
{
    public function testList()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $client->loginUser($testUser);

        $dealRepository = static::$container->get(DealRepository::class);
        /** @var Deal $deal */
        $deal = $dealRepository->findOneBy(['company' => $testUser->getCompanies()->toArray()]);

        $client->request(
            'GET',
            '/api/note/' . $deal->getId(),
            ['limit' => $deal->getNotes()->count(), 'offset' => 0],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        foreach ($deal->getNotes() as $note) {
            $this->assertStringContainsString(sprintf('"text":"%s"', $note->getText()), $client->getResponse()->getContent());
        }
    }

    public function testListNotFoundDeal()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $client->loginUser($testUser);

        $dealRepository = static::$container->get(DealRepository::class);
        /** @var Deal $deal */
        $deal = $dealRepository->findOneBy(['company' => $testUser->getCompanies()->toArray()], ['id' => 'DESC']);

        $client->request(
            'GET',
            '/api/note/' . ($deal->getId() + 1),
            ['limit' => 1, 'offset' => 0],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testListUnauthorized()
    {
        $client = static::createClient();
        /** @var Deal $deal */
        $deal = static::$container->get(DealRepository::class)->findOneBy([]);
        $client->request('GET', sprintf('/api/note/%s', $deal->getId()));

        $this->assertEquals(401, $client->getResponse()->getStatusCode());
    }

    public function testCreate()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $client->loginUser($testUser);

        $dealRepository = static::$container->get(DealRepository::class);
        $deal = $dealRepository->findOneBy(['company' => $testUser->getCompanies()->toArray()]);;

        $client->request(
            'POST',
            '/api/note/create',
            [
                "text" => "Test create note",
                "deal_id" => $deal->getId(),
            ],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $noteRepository = static::$container->get(NoteRepository::class);
        /** @var Note|null $testNote */
        $testNote = $noteRepository->findOneBy([
            'text' => 'Test create note',
            'deal' => $deal,
        ], ['id' => 'DESC']);

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertStringContainsString('"text":"Test create note"', $client->getResponse()->getContent());
        $this->assertStringContainsString('"dealId":' . $deal->getId(), $client->getResponse()->getContent());
        $this->assertNotEmpty($testNote);
        if ($testNote) {
            $this->assertEquals($deal->getId(), $testNote->getDeal()->getId());
        }
    }

    public function testCreateNotFoundDeal()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $client->loginUser($testUser);

        $dealRepository = static::$container->get(DealRepository::class);
        $deal = $dealRepository->findOneBy(['company' => $testUser->getCompanies()->toArray()], ['id' => 'DESC']);

        $client->request(
            'POST',
            '/api/note/create',
            [
                "text" => "Test create note",
                "deal_id" => $deal->getId() + 1,
            ],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertTrue($client->getResponse()->isNotFound());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testCreateBadRequest()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $client->loginUser($testUser);

        $client->request(
            'POST',
            '/api/note/create',
            [],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testCreateUnauthorized()
    {
        $client = static::createClient();
        $client->request('POST', '/api/note/create');

        $this->assertEquals(401, $client->getResponse()->getStatusCode());
    }

    public function testDelete()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $client->loginUser($testUser);

        $dealRepository = static::$container->get(DealRepository::class);
        /** @var Deal $deal */
        $deal = $dealRepository->findOneBy(['company' => $testUser->getCompanies()->toArray()], ['id' => 'DESC']);
        $noteRepository = static::$container->get(NoteRepository::class);
        $note = $deal->getNotes()->first();
        $noteId = $note->getId();

        $client->request(
            'POST',
            '/api/note/delete/' . $note->getId(),
            [],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $deletedNote = $noteRepository->find($noteId);

        $this->assertResponseIsSuccessful();
        $this->assertEmpty($deletedNote);
    }

    public function testDeleteNotFound()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $client->loginUser($testUser);

        $noteRepository = static::$container->get(NoteRepository::class);
        $note = $noteRepository->findOneBy([], ['id' => 'DESC']);

        $client->request(
            'POST',
            '/api/note/delete/' . ($note->getId() + 1),
            [],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertTrue($client->getResponse()->isNotFound());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testDeleteUnauthorized()
    {
        $client = static::createClient();
        $client->request('POST', '/api/note/delete/1');

        $this->assertEquals(401, $client->getResponse()->getStatusCode());
    }
}
