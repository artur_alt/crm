<?php


namespace App\Tests\Controller;


use App\Entity\Company;
use App\Entity\Contact;
use App\Entity\Deal;
use App\Entity\User;
use App\Repository\ContactRepository;
use App\Repository\DealRepository;
use App\Repository\UserRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ContactControllerTest extends WebTestCase
{
    public function testUpdateUnauthorized()
    {
        $client = static::createClient();
        $client->request('POST', '/api/contact/update');

        $this->assertEquals(401, $client->getResponse()->getStatusCode());
    }

    public function testUpdate()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $client->loginUser($testUser);

        $contactRepository = static::$container->get(ContactRepository::class);
        /** @var Contact $contact */
        $contact = $contactRepository->findOneBy(
            ['company' => $testUser->getCompanies()->first()],
            ['id' => 'DESC']
        );

        $newEmail = sprintf('newperded%d%d@lol.lol', rand(1,100), rand(1,100));

        $client->request(
            'POST',
            '/api/contact/update',
            [
                "id" => $contact->getId(),
                "name" => "Test update contact",
                "phone" => "89771112200",
                "address" => "Test contact address",
                "email" => $newEmail,
            ],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        /** @var Contact|null $testContact */
        $testContact = $contactRepository->findOneBy(["email" => $newEmail], ['id' => 'DESC']);

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertStringContainsString('"id":' . $contact->getId(), $client->getResponse()->getContent());
        $this->assertStringContainsString('"name":"Test update contact"', $client->getResponse()->getContent());
        $this->assertStringContainsString('"phone":"89771112200"', $client->getResponse()->getContent());
        $this->assertStringContainsString('"address":"Test contact address"', $client->getResponse()->getContent());
        $this->assertStringContainsString(sprintf('"email":"%s"', $newEmail), $client->getResponse()->getContent());
        $this->assertNotEmpty($testContact);
        if ($testContact) {
            $this->assertEquals('Test update contact', $testContact->getName());
        }
    }

    public function testUpdateNotFound()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $client->loginUser($testUser);

        $contactRepository = static::$container->get(ContactRepository::class);
        $contact = $contactRepository->findOneBy([], ['id' => 'DESC']);

        $client->request(
            'POST',
            '/api/contact/update',
            [
                "id" => $contact->getId() + 1,
                "name" => "Test update contact not found",
            ],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertTrue($client->getResponse()->isNotFound());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testUpdateNotFoundContactFromAnotherCompany()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $testUser2 = $userRepository->findOneBy(['email' => 'test1@test.test']);
        $client->loginUser($testUser);

        $dealRepository = static::$container->get(DealRepository::class);
        /** @var Deal $deal */
        $deal = $dealRepository->findOneBy(
            ['company' => array_map(fn(Company $c) => $c->getId(), $testUser2->getCompanies()->toArray())],
            ['id' => 'DESC']
        );
        /** @var Contact $contact */
        $contact = $deal->getContacts()->first();

        $client->request(
            'POST',
            '/api/contact/update',
            [
                "id" => $contact->getId(),
                "name" => "Test update contact not found 2",
            ],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertTrue($client->getResponse()->isNotFound());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testUpdateBadRequest()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $client->loginUser($testUser);

        $dealRepository = static::$container->get(DealRepository::class);
        /** @var Deal $deal */
        $deal = $dealRepository->findOneBy(
            ['company' => array_map(fn(Company $c) => $c->getId(), $testUser->getCompanies()->toArray())],
            ['id' => 'DESC']
        );
        /** @var Contact $contact */
        $contact = $deal->getContacts()->first();

        $client->request(
            'POST',
            '/api/contact/update',
            [
                "id" => $contact->getId(),
                "email" => 'I am not an email! LOL!',
            ],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testDelete()
    {
        $client = static::createClient();
        $entityManager = $client->getContainer()
            ->get('doctrine')
            ->getManager();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $client->loginUser($testUser);

        $contactRepository = static::$container->get(ContactRepository::class);
        $dealRepository = static::$container->get(DealRepository::class);
        /** @var Deal $deal */
        $deal = $dealRepository->findOneBy(
            ['company' => array_map(fn(Company $c) => $c->getId(), $testUser->getCompanies()->toArray())],
            ['id' => 'DESC']
        );

        // add new contact for delete
        $newContact = (new Contact($deal->getCompany()))
            ->setName('Contact delete')
            ->setEmail('contactdelete@test.test')
            ->setPhone('89771235050')
            ->setCreatedAt(new DateTime())
            ->setUpdatedAt(new DateTime())
            ->setAddress('Los Santos');

        $deal->addContact($newContact);

        $entityManager->persist($newContact);
        $entityManager->persist($deal);
        $entityManager->flush();
        $entityManager->clear();

        $contactId = $newContact->getId();

        $client->request(
            'POST',
            '/api/contact/delete/' . $newContact->getId(),
            [],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $deletedContact = $contactRepository->find($contactId);

        $this->assertResponseIsSuccessful();
        $this->assertEmpty($deletedContact);

        $entityManager->close();
        $entityManager = null;
    }

    public function testDeleteNotFound()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $client->loginUser($testUser);

        $contactRepository = static::$container->get(ContactRepository::class);
        /** @var Contact $contact */
        $contact = $contactRepository->findOneBy([], ['id' => 'DESC']);

        $client->request(
            'POST',
            '/api/contact/delete/' . ($contact->getId() + 1),
            [],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertTrue($client->getResponse()->isNotFound());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testDeleteNotFoundContactFromAnotherCompany()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $testUser2 = $userRepository->findOneBy(['email' => 'test1@test.test']);
        $client->loginUser($testUser);

        $dealRepository = static::$container->get(DealRepository::class);
        /** @var Deal $deal */
        $deal = $dealRepository->findOneBy(
            ['company' => array_map(fn(Company $c) => $c->getId(), $testUser2->getCompanies()->toArray())],
            ['id' => 'DESC']
        );
        /** @var Contact $contact */
        $contact = $deal->getContacts()->first();

        $client->request(
            'POST',
            '/api/contact/delete/' . $contact->getId(),
            [],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertTrue($client->getResponse()->isNotFound());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testDeleteUnauthorized()
    {
        $client = static::createClient();
        $client->request('POST', '/api/contact/delete/1');

        $this->assertEquals(401, $client->getResponse()->getStatusCode());
    }
}
