<?php


namespace App\Tests\Controller;


use App\Entity\Company;
use App\Entity\Deal;
use App\Entity\User;
use App\Repository\ContactRepository;
use App\Repository\DealRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DealControllerTest extends WebTestCase
{
    public function testList()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test1@test.test']);
        $client->loginUser($testUser);

        $client->request(
            'GET',
            '/api/deal',
            ['limit' => 3, 'offset' => 0, 'companyId' => $testUser->getCompanies()->first()->getId()],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertStringContainsString('"name":"Deal 1"', $client->getResponse()->getContent());
        $this->assertStringContainsString('"name":"Contact 1 0"', $client->getResponse()->getContent());
        $this->assertStringContainsString('"name":"Contact 1 1"', $client->getResponse()->getContent());
    }

    public function testListBadRequest()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test1@test.test']);
        $client->loginUser($testUser);

        $client->request(
            'GET',
            '/api/deal',
            ['limit' => 3, 'offset' => 0],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }


    public function testListWithWrongCompany()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test1@test.test']);
        $client->loginUser($testUser);

        $client->request(
            'GET',
            '/api/deal',
            [
                'limit' => 3,
                'offset' => 0,
                'companyId' => 1 + max(array_map(fn(Company $c) => $c->getId(), $testUser->getCompanies()->toArray())),
            ],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertEquals('[]', $client->getResponse()->getContent());
    }

    public function testListUnauthorized()
    {
        $client = static::createClient();
        $client->request('GET', '/api/deal');

        $this->assertEquals(401, $client->getResponse()->getStatusCode());
    }

    public function testGetById()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $client->loginUser($testUser);

        $dealRepository = static::$container->get(DealRepository::class);
        $deal = $dealRepository->findOneBy(['company' => $testUser->getCompanies()->toArray()], ['id' => 'DESC']);

        $client->request(
            'GET',
            sprintf('/api/deal/%d', $deal->getId()),
            [],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertStringContainsString(sprintf('"name":"%s"', $deal->getName()), $client->getResponse()->getContent());
    }

    public function testGetByIdNotFound()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $client->loginUser($testUser);

        $dealRepository = static::$container->get(DealRepository::class);
        $deal = $dealRepository->findOneBy([], ['id' => 'DESC']);

        $client->request(
            'GET',
            sprintf('/api/deal/%d', $deal->getId() + 1),
            [],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertTrue($client->getResponse()->isNotFound());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testGetByIdUnauthorized()
    {
        $client = static::createClient();
        $client->request('GET', '/api/deal/1');

        $this->assertEquals(401, $client->getResponse()->getStatusCode());
    }

    public function testCreate()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test1@test.test']);
        $client->loginUser($testUser);

        $contactRepository = static::$container->get(ContactRepository::class);
        $contact = $contactRepository->findOneBy(['company' => $testUser->getCompanies()->first()->getId()]);

        $client->request(
            'POST',
            '/api/deal/create',
            [
                "name" => "Test create deal",
                "status" => "new",
                "companyId" => $testUser->getCompanies()->last()->getId(),
                "contacts" => [
                    [
                        "name" => "Test contact 1",
                        "phone" => "89771112222",
                        "address" => "Тест город",
                        "email" => "perded@lol.lol",
                    ],
                    [
                        "id" => $contact->getId(),
                    ]
                ]
            ],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $dealRepository = static::$container->get(DealRepository::class);
        /** @var Deal|null $testDeal */
        $testDeal = $dealRepository->findOneBy(['name' => 'Test create deal'], ['id' => 'DESC']);

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertStringContainsString('"name":"Test create deal"', $client->getResponse()->getContent());
        $this->assertStringContainsString('"name":"Test contact 1"', $client->getResponse()->getContent());
        $this->assertStringContainsString(sprintf('"name":"%s"', $contact->getName()), $client->getResponse()->getContent());
        $this->assertNotEmpty($testDeal);
        if ($testDeal) {
            $this->assertFalse($testDeal->getContacts()->isEmpty());
            $this->assertTrue($testUser->hasCompany($testDeal->getCompany()->getId()));
        }
    }

    public function testCreateUnauthorized()
    {
        $client = static::createClient();
        $client->request('POST', '/api/deal/create');

        $this->assertEquals(401, $client->getResponse()->getStatusCode());
    }

    public function testCreateBadRequest()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $client->loginUser($testUser);

        $client->request(
            'POST',
            '/api/deal/create',
            [],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testCreateNotFoundContact()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $client->loginUser($testUser);

        $contactRepository = static::$container->get(ContactRepository::class);
        $contact = $contactRepository->findOneBy([], ['id' => 'DESC']);

        $client->request(
            'POST',
            '/api/deal/create',
            [
                "name" => "Test create deal",
                "status" => "new",
                "companyId" => $testUser->getCompanies()->last()->getId(),
                "contacts" => [
                    [
                        "id" => $contact->getId() + 1,
                    ]
                ]
            ],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertTrue($client->getResponse()->isNotFound());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testCreateNotFoundCompany()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $client->loginUser($testUser);

        $contactRepository = static::$container->get(ContactRepository::class);
        $contact = $contactRepository->findOneBy(
            ['company' => $testUser->getCompanies()->first()->getId()],
            ['id' => 'DESC']
        );

        $client->request(
            'POST',
            '/api/deal/create',
            [
                "name" => "Test create deal",
                "status" => "new",
                "companyId" => 1 + max(array_map(fn(Company $c) => $c->getId(), $testUser->getCompanies()->toArray())),
                "contacts" => [
                    [
                        "id" => $contact->getId(),
                    ]
                ]
            ],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertTrue($client->getResponse()->isNotFound());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testUpdateUnauthorized()
    {
        $client = static::createClient();
        $client->request('POST', '/api/deal/update');

        $this->assertEquals(401, $client->getResponse()->getStatusCode());
    }

    public function testUpdateNotFound()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $client->loginUser($testUser);

        $dealRepository = static::$container->get(DealRepository::class);
        $deal = $dealRepository->findOneBy([], ['id' => 'DESC']);

        $client->request(
            'POST',
            '/api/deal/update',
            [
                "id" => $deal->getId() + 1,
                "name" => "test",
                "status" => "hot",
                "companyId" => $deal->getCompany()->getId(),
            ],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertTrue($client->getResponse()->isNotFound());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testUpdateNotFoundCompany()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $client->loginUser($testUser);

        $dealRepository = static::$container->get(DealRepository::class);
        $deal = $dealRepository->findOneBy(['company' => $testUser->getCompanies()->toArray()], ['id' => 'DESC']);

        $client->request(
            'POST',
            '/api/deal/update',
            [
                "id" => $deal->getId(),
                "name" => "test",
                "status" => "hot",
                "companyId" => 1 + max(array_map(fn(Company $c) => $c->getId(), $testUser->getCompanies()->toArray())),
            ],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertTrue($client->getResponse()->isNotFound());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testUpdateBadRequest()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $client->loginUser($testUser);

        $client->request(
            'POST',
            '/api/deal/update',
            [],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testUpdate()
    {
        $client = static::createClient();
        $em = static::$container->get('doctrine.orm.entity_manager');
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test1@test.test']);
        $client->loginUser($testUser);

        $dealRepository = static::$container->get(DealRepository::class);
        /** @var Deal $deal */
        $deal = $dealRepository->findOneBy(['company' => $testUser->getCompanies()->first()->getId()], ['id' => 'DESC']);
        if(!$deal->getContacts()->isEmpty()) {
            // clear contacts
            $deal->setContacts(new ArrayCollection());
            $em->persist($deal);
            $em->flush();
        }

        $contactRepository = static::$container->get(ContactRepository::class);
        $contact = $contactRepository->findOneBy(['company' => $testUser->getCompanies()->first()->getId()]);

        $client->request(
            'POST',
            '/api/deal/update',
            [
                "id" => $deal->getId(),
                "name" => "Test update deal",
                "status" => "hot",
                "companyId" => $testUser->getCompanies()->last()->getId(),
                "contacts" => [
                    [
                        "name" => "Test update deal new contact 1",
                        "phone" => "89771112222",
                        "address" => "Тест город",
                        "email" => "perded@lol.lol",
                    ],
                    [
                        "id" => $contact->getId(),
                    ]
                ]
            ],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $testDeal = $dealRepository->findOneBy(['name' => 'Test update deal'], ['id' => 'DESC']);

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertStringContainsString('"id":' . $deal->getId(), $client->getResponse()->getContent());
        $this->assertStringContainsString('"name":"Test update deal"', $client->getResponse()->getContent());
        $this->assertStringContainsString('"name":"Test update deal new contact 1"', $client->getResponse()->getContent());
        $this->assertStringContainsString(sprintf('"id":%s', $contact->getId()), $client->getResponse()->getContent());
        $this->assertStringContainsString(sprintf('"name":"%s"', $contact->getName()), $client->getResponse()->getContent());
        $this->assertNotEmpty($testDeal);
        if ($testDeal) {
            $this->assertFalse($testDeal->getContacts()->isEmpty());
        }
    }

    public function testUpdateNotFoundContact()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $client->loginUser($testUser);

        $dealRepository = static::$container->get(DealRepository::class);
        $deal = $dealRepository->findOneBy(['company' => $testUser->getCompanies()->first()->getId()], ['id' => 'DESC']);

        $contactRepository = static::$container->get(ContactRepository::class);
        $contact = $contactRepository->findOneBy(['company' => $testUser->getCompanies()->first()->getId()], ['id' => 'DESC']);

        $client->request(
            'POST',
            '/api/deal/update',
            [
                "id" => $deal->getId(),
                "name" => "Test update deal 2",
                "status" => "hot",
                "companyId" => $testUser->getCompanies()->last()->getId(),
                "contacts" => [
                    [
                        "id" => $contact->getId() + 1,
                    ]
                ]
            ],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertTrue($client->getResponse()->isNotFound());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }
}
