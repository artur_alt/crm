<?php


namespace App\Tests\Controller;


use App\Entity\User;
use App\Repository\UserRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    public function testLogin()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/user/login',
            [
                "email" => "test0@test.test",
                "password" => "the_new_password",
            ]
        );

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertStringContainsString('"token":', $client->getResponse()->getContent());
    }

    public function testLoginBadRequest()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/user/login'
        );

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testLoginNotFound()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/user/login',
            [
                "email" => md5('test0') . "test0@test.test",
                "password" => "the_new_password",
            ]
        );

        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testLoginWrongPassword()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/user/login',
            [
                "email" => "test0@test.test",
                "password" => md5('test0') . "the_new_password",
            ]
        );

        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testCreate()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $client->loginUser($testUser);

        $email = sprintf("test%s@test.test", (new DateTime())->getTimestamp());

        $client->request(
            'POST',
            '/api/user/create',
            [
                "email" => $email,
                "firstName" => "Create",
                "lastName" => "Test",
                "phone" => "89711231244",
                "password" => "the_new_password",
            ],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()],
        );

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertStringContainsString(sprintf('"email":"%s"', $email), $client->getResponse()->getContent());
    }

    public function testCreateUnauthorized()
    {
        $client = static::createClient();
        $email = sprintf("test%s@test.test", (new DateTime())->getTimestamp());

        $client->request(
            'POST',
            '/api/user/create',
            [
                "email" => $email,
                "firstName" => "Create",
                "lastName" => "Test",
                "phone" => "89711231244",
                "password" => "the_new_password",
            ]
        );

        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testCreateBadRequest()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $client->loginUser($testUser);

        $client->request(
            'POST',
            '/api/user/create',
            [],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()],
        );

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    public function testCreateAlreadyExists()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        /** @var User $testUser */
        $testUser = $userRepository->findOneBy(['email' => 'test0@test.test']);
        $client->loginUser($testUser);

        $client->request(
            'POST',
            '/api/user/create',
            [
                "email" => "test1@test.test",
                "firstName" => "CreateAlreadyExists",
                "lastName" => "Test",
                "phone" => "89711231244",
                "password" => "the_new_password",
            ],
            [],
            ['HTTP_X-AUTH-TOKEN' => $testUser->getApiToken()]
        );

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }
}
